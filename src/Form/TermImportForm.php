<?php


namespace Drupal\senapi_content\Form;


use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\VocabularyStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TermImportForm extends FormBase {
  /**
   * Set a var to make stepthrough form.
   *
   * @var step
   */
  protected $step = 1;

  /**
   * Keep track of user input.
   *
   * @var userInput
   */
  protected $userInput = [];

  /**
   * The vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  public function __construct(VocabularyStorageInterface $vocabulary_storage) {
     $this->vocabularyStorage = $vocabulary_storage;
  }

  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity.manager')->getStorage('taxonomy_vocabulary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'term_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t('CSV Term Import');
    $value = '';
    switch ($this->step) {
      case 1:
        $form['input'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Input'),
          '#description' => $this->t('<p><strong>See CSV Export for an example.</strong></p><p>Enter in the form of: <pre>"name,status,description,format,weight,parent_name,[any_additional_fields];</pre><pre>name,status,description,format,weight,parent_name[;parent_name1;parent_name2;...],[any_additional_fields]"</pre> or <pre>"tid,uuid,name,status,revision_id,description,format,weight,parent_name[;parent_name1;parent_name2;...],parent_tid[;parent_tid1;parent_tid2;...],[any_additional_fields];</pre><pre>tid,uuid,name,status,revision_id,description,format,weight,parent_name,parent_tid,[any_additional_fields]"</pre> Note that <em>[any_additional_fields]</em> are optional and are stringified using <a href="http://www.php.net/http_build_query">http_build_query</a>.</p>'),
        ];
        $form['preserve_vocabularies'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Preserve Vocabularies on existing terms.'),
        ];
        $vocabularies = taxonomy_vocabulary_get_names();
        $vocabularies['create_new'] = 'create_new';
        $form['vocabulary'] = [
          '#type' => 'select',
          '#title' => $this->t('Taxonomy'),
          '#options' => $vocabularies,
        ];
        $value = $this->t('Next');
        break;

      case 2:
        $form['name'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Name'),
          '#maxlength' => 255,
          '#required' => TRUE,
        ];
        $form['vid'] = [
          '#type' => 'machine_name',
          '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
          '#machine_name' => [
            'exists' => [$this, 'exists'],
            'source' => ['name'],
          ],
        ];
        $form['#title'] .= ' - ' . $this->t('Create New Vocabulary');
        $value = $this->t('Create Vocabulary');
        break;

      case 3:
        $preserve = '';
        if ($this->userInput['preserve_vocabularies']) {
          $preserve = " and preserve vocabularies on existing terms";
        }
        $has_header = stripos($this->userInput['input'], "name,status,description__value,description__format,weight,parent_name");
        $term_count = count(array_filter(preg_split('/\r\n|\r|\n/', $this->userInput['input'])));
        if ($has_header !== false) {
          $term_count = $term_count -1;
        }
        $form['#title'] .= ' - ' . $this->t('Are you sure you want to copy @count_terms terms into the vocabulary @vocabulary@preserve_vocabularies?',
                                     [
                                       '@count_terms' => $term_count,
                                       '@vocabulary' => $this->userInput['vocabulary'],
                                       '@preserve_vocabularies' => $preserve,
                                     ]);
        $value = $this->t('Import');
        break;
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $value,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    switch ($this->step) {
      case 1:
        if ($form_state->getValue('vocabulary') != 'create_new') {
          $this->step++;
          $this->userInput['vocabulary'] = $form_state->getValue('vocabulary');
        }
        $this->userInput['preserve_vocabularies'] = $form_state->getValue('preserve_vocabularies');
        $this->userInput['input'] = $form_state->getValue('input');
        $form_state->setRebuild();
        break;

      case 2:
        $vocabulary = $this->createVocab($form_state->getValue('vid'), $form_state->getValue('name'));
        $this->userInput['vocabulary'] = $vocabulary;
        $form_state->setRebuild();
        break;

      case 3:
        $dataStore = [];
        $this->import($this->userInput['input'], $dataStore);
        $this->execute($this->userInput['preserve_vocabularies'], $this->userInput['vocabulary'], $dataStore);
        break;
    }
    $this->step++;
  }

  /**
   * {@inheritdoc}
   */
  public function createVocab($vid, $name) {
    $vocabulary = Vocabulary::create([
      'vid' => $vid,
      'machine_name' => $vid,
      'name' => $name,
    ]);
    $vocabulary->save();
    return $vocabulary->id();
  }

  /**
   * Determines if the vocabulary already exists.
   *
   * @param string $vid
   *   The vocabulary ID.
   *
   * @return bool
   *   TRUE if the vocabulary exists, FALSE otherwise.
   */
  public function exists($vid) {
    $action = $this->vocabularyStorage->load($vid);
    return !empty($action);
  }

  public function import($data, &$dataStore) {
    $temp = fopen('php://memory', 'rw');
    fwrite($temp, $data);
    rewind($temp);
    $csvArray = [];
    while (!feof($temp)) {
      if ($csvRow = fgetcsv($temp)) {
        $csvArray[] = $csvRow;
      }
    }
    fclose($temp);
    $keys_noid = ['name', 'status', 'description__value', 'description__format', 'weight', 'parent_name'];
    $keys_id = [
      'tid',
      'uuid',
      'name',
      'status',
      'revision_id',
      'description__value',
      'description__format',
      'weight',
      'parent_name',
      'parent_tid',
    ];
    $keys = [];
    $may_need_revision = true;
    if (!array_diff($keys_noid, $csvArray[0])) {
      \Drupal::messenger()->addWarning(t('The header keys were not included in the import.'));
      $keys = $csvArray[0];
      if (isset($keys['revision_id'])) {
        // This is not an export from an earlier version.
        $may_need_revision = false;
      }
      unset($csvArray[0]);
    }
    foreach ($csvArray as $csvLine) {
      $num_of_lines = count($csvLine);
      $needs_revision = false;
      if (in_array($num_of_lines, [9, 10]) && $may_need_revision) {
        // This export may be from an earlier version. Check for revision_id.
        if (!is_numeric($csvLine[4])) {
          // The default revision_id in 8.7 is the tid.
          array_splice($csvLine, 4, 0, $csvLine[0]);
          $needs_revision = true;
          $num_of_lines += 1;
        }
      }
      if (empty($keys)) {
        if (in_array($num_of_lines, [10, 11])) {
          $keys = $keys_id;
        }
        elseif (in_array($num_of_lines, [6, 7])) {
          $keys = $keys_noid;
        }
        else {
          \Drupal::messenger()->addError(t('Line with "@part" could not be parsed. Incorrect number of values: @count.',
            [
              '@part' => implode(',', $csvLine),
              '@count' => count($csvLine),
            ]));
          continue;
        }
        if (in_array($num_of_lines, [7, 11])) {
          $keys[] = 'fields';
        }
      }
      if ($needs_revision && !in_array('revision_id', $keys)) {
        array_splice($keys, 4, 0, 'revision_id');
      }
      $dataStore[] = array_combine($keys, $csvLine);
    }
  }

  public function execute($preserve_vocabularies, $vocabulary, $dataStore) {
    Cache::invalidateTags(array(
      'taxonomy_term_values',
    ));
    $processed = 0;
    // TODO Inject.
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    foreach ($dataStore as $row) {
      //remove whitespace
      foreach ($row as $key => $value) {
        $row[$key] = trim($value);
      }
      // Check for existence of terms.
      if (isset($row['tid'])) {
        $term_existing = Term::load($row['tid']);
      }
      else {
        $term_existing = taxonomy_term_load_multiple_by_name($row['name'], $vocabulary);
        if (count($term_existing) > 1) {
          \Drupal::messenger()->addMessage(t('The term @name has multiple matches. Ignoring.', ['@name' => $row['name']]));
          continue;
        }
        else if (count($term_existing) == 1) {
          $term_existing = $term_existing[0];
        }
      }
      if ($term_existing) {
        $new_term = $term_existing;
      }
      // Or create the term.
      elseif (isset($row['tid'])) {
        // Double check for Term ID cause this could go bad.
        $db = Database::getConnection();
        $query = $db->select('taxonomy_term_data')
          ->fields('taxonomy_term_data', ['tid'])
          ->condition('taxonomy_term_data.tid', $row['tid'], '=');
        $tids = $query->execute()->fetchAll(\PDO::FETCH_OBJ);
        $query1 = $db->select('taxonomy_term_field_data')
          ->fields('taxonomy_term_field_data', ['tid'])
          ->condition('taxonomy_term_field_data.tid', $row['tid'], '=');
        $tids1 = $query1->execute()->fetchAll(\PDO::FETCH_OBJ);
        if (!empty($tids) || !empty($tids1)) {
          \Drupal::messenger()->addError(t('The Term ID already exists.'));
          continue;
        }
        $db->insert('taxonomy_term_data')
          ->fields([
            'tid' => $row['tid'],
            'vid' => $vocabulary,
            'uuid' => $row['uuid'],
            'revision_id' => $row['revision_id'],
            'langcode' => $langcode,
          ])
          ->execute();
        $db->insert('taxonomy_term_field_data')
          ->fields([
            'tid' => $row['tid'],
            'vid' => $vocabulary,
            'status' => $row['status'],
            'revision_id' => $row['revision_id'],
            'name' => $row['name'],
            'langcode' => $langcode,
            'default_langcode' => 1,
            'weight' => $row['weight'],
            'revision_translation_affected' => 1,
          ])
          ->execute();
        $db->insert('taxonomy_term_revision')
          ->fields([
            'tid' => $row['tid'],
            'revision_id' => $row['revision_id'],
            'langcode' => $langcode,
            'revision_default' => 1,
          ])
          ->execute();
        $db->insert('taxonomy_term_field_revision')
          ->fields([
            'tid' => $row['tid'],
            'status' => $row['status'],
            'revision_id' => $row['revision_id'],
            'name' => $row['name'],
            'langcode' => $langcode,
            'default_langcode' => 1,
            'revision_translation_affected' => 1,
          ])
          ->execute();
        $new_term = Term::load($row['tid']);
      }
      else {
        $new_term = Term::create(['name' => $row['name'], 'vid' => $vocabulary, 'status' => $row['status'], 'langcode' => $langcode]);
      }
      // Change the vocabulary if requested.
      if ($new_term->getVocabularyId() != $vocabulary && !$preserve_vocabularies) {
        // TODO: Make this work.
        // $new_term->vid->setValue($vocabulary);
        // Currently get an EntityStorageException when field does not exist in new vocab.
        // TODO: Save the term so fields are set properly when above todo done.
        // $new_term->save();
        // So, we update the db instead.
        $tid = $new_term->id();
        $db = Database::getConnection();
        $db->update('taxonomy_term_data')
          ->fields(['vid' => $vocabulary])
          ->condition('tid', $tid, '=')
          ->execute();
        $db->update('taxonomy_term_field_data')
          ->fields(['vid' => $vocabulary])
          ->condition('tid', $tid, '=')
          ->execute();
        \Drupal::entityTypeManager()->getStorage('taxonomy_term')->resetCache([$tid]);
        $new_term = Term::load($tid);
      }
      // Set temp parents.
      $parent_terms = NULL;
      if (!empty($row['parent_tid'])) {
        if (strpos($row['parent_tid'],';') !== FALSE) {
          $parent_tids =  array_filter(explode(';',$row['parent_tid']), 'strlen');
          foreach ($parent_tids as $parent_tid) {
            $parent_terms[] = Term::load($parent_tid);
          }
        }
        else {
          $parent_terms[] = Term::load($row['parent_tid']);
        }
      }
      $new_term->setDescription($row['description__value'])
        ->setName($row['name'])
        ->set('status', $row['status'])
        ->set('langcode', $langcode)
        ->setFormat($row['description__format'])
        ->setWeight($row['weight']);
      // Check for parents.
      if ($parent_terms == NULL && !empty($row['parent_name'])) {
        $parent_names = explode(';',$row['parent_name']);
        foreach ($parent_names as $parent_name) {
          $parent_term = taxonomy_term_load_multiple_by_name($parent_name, $vocabulary);
          if (count($parent_term) > 1) {
            unset($parent_term);
            \Drupal::messenger()->addError(t('More than 1 terms are named @name. Cannot distinguish by name. Try using id export/import.', ['@name' => $row['parent_name']]));
          }
          else {
            $parent_terms[] = array_values($parent_term)[0];
          }
        }
      }
      if ($parent_terms) {
        $parent_terms = array_filter($parent_terms);
        $parent_tids = [];
        foreach ($parent_terms as $parent_term) {
          $parent_tids[] = $parent_term->id();
        }
        $new_term->parent = $parent_tids;
      }
      else {
        $new_term->parent = 0;
      }

      // Import all other non-default taxonomy fields if the row is there.
      if (isset($row['fields']) && !empty($row['fields'])) {
        parse_str($row['fields'], $field_array);
        if (!is_array($field_array)) {
          \Drupal::messenger()->addError(t('The field data <em>@data</em> is not formatted correctly. Please use the export function.', ['@data' => $row['fields']]));
          continue;
        }
        else {
          foreach ($field_array as $field_name => $field_values) {
            if ($new_term->hasField($field_name)) {
              $new_term->set($field_name, $field_values);
            }
            else {
              \Drupal::messenger()->addWarning(t('The field data <em>@data</em> could not be imported. Please add the appropriate fields to the vocabulary you are importing into.', ['@data' => $row['fields']]));
            }
          }
        }
      }

      $new_term->save();
      $processed++;
    }
    \Drupal::messenger()->addMessage(t('Imported @count terms.', ['@count' => $processed]));
  }
}