<?php

namespace Drupal\senapi_content\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\senapi_content\ImportHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WebDriver\Exception;

class NodeImportForm extends FormBase {

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * A instance of the senapi_content helper services.
   *
   * @var \Drupal\senapi_content\ImportHelper
   */
  protected $entityHelper;

  /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    FileSystem $file_system,
    ImportHelper $entityHelper,
    EntityTypeManagerInterface $entityTypeManager) {

    $this->fileSystem = $file_system;
    $this->entityHelper = $entityHelper;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('senapi_content.import_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['upload_csv'] = [
      '#type' => 'managed_file',
      '#title' => 'Seleccione archivo CSV',
      '#description' => 'Por favor cargue el archivo CSV generado por la acción exportar.',
      '#upload_location' => $this->entityHelper->folders['import_data']['relative'],
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#required' => TRUE,
    ];

    $form['upload_zip'] = [
      '#type' => 'managed_file',
      '#title' => 'Seleccione archivo ZIP',
      '#description' => 'Por favor cargue el archivo ZIP generado por la acción exportar.',
      '#upload_location' => $this->entityHelper->folders['import_media']['relative'],
      '#upload_validators' => [
        'file_validate_extensions' => ['zip'],
      ],
    ];

    $contentTypes = $this->entityHelper->getAllContentTypes();
    $selected = !empty($form_state->getValue('content_types')) ? $form_state->getValue('content_types') : key($contentTypes);
    $form['content_types'] = [
      '#type' => 'select',
      '#title' => 'Tipo de Contenido',
      '#description' => 'Por favor seleccione tipo de contenido para importar.',
      '#default_value' => $selected,
      '#options' => $contentTypes,
      '#states' => [
      ],
      '#ajax' => [
        'callback' => '::ajaxDependentDropdownCallback',
        'wrapper' => 'dropdown-second-replace',
      ],
      '#required' => TRUE,
    ];

    $options = $this->entityHelper->getAllItemFields($selected, 'node');

    $form['content_types_fields'] = [
      '#type' => 'select',
      '#title' => $contentTypes[$selected] . ' campos',
      '#default_value' => $selected,
      '#options' => $options,
      '#multiple' => TRUE,
      '#size' => 10,
      '#states' => [
      ],
      '#prefix' => '<div id="dropdown-second-replace">',
      '#suffix' => '</div>',
      '#attributes' => [
        'disabled' => TRUE,
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $uploadCsv = $form_state->getValue('upload_csv');
    if (empty($uploadCsv)) {
      $form_state->setErrorByName('upload_csv', 'Por favor cargue un archivo CSV.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $contentTypes = $form_state->getValue('content_types');
    $fields = $this->entityHelper->getFields($contentTypes);
    $fieldNames = $fields['name'];
    $fieldProperties = $fields['properties'];

    $storage = $this->entityTypeManager->getStorage('file');

    $uploadZip = $form_state->getValue('upload_zip');
    $uploadZipFile = FALSE;
    if (count($uploadZip)) {
      $uploadZipFile = $storage->load($uploadZip[0]);
      $archiver = $this->entityHelper->getArchiver($this->fileSystem->realpath($uploadZipFile->getFileUri()));
      $archiver->extractTo($this->entityHelper->folders['import_media']['relative']);
    }

    $dirExtract = $this->entityHelper->folders['import_media']['relative'] . '/' . $contentTypes;
    if (!is_dir($dirExtract)) {
      $dirExtract = $this->entityHelper->folders['import_media']['relative'];
    }

    $uploadCsv = $form_state->getValue('upload_csv');
    $uploadCsvFile = $storage->load($uploadCsv[0]);
    $handle = $this->entityHelper->getFileHandler($uploadCsvFile->getFileUri());

    $nodeArray = [];

    $header = $this->entityHelper->getCsvData($handle);

    unset($fields['requireField']['type']);
    foreach ($fields['requireField'] as $field) {
      if (!in_array($field, $header)) {
        \Drupal::messenger()
          ->addError("Error al importar campos requeridos incompletos.");
        return;
      }
    }

    $date = new \DateTime();

    while ($data = $this->entityHelper->getCsvData($handle)) {
      $data = array_combine($header, $data);

      foreach ($data as $dataKey => $dataValue) {
        if (in_array($dataKey, $fieldNames)) {
          $prop = $fieldProperties[$dataKey];

          switch ($prop['type']) {
            case 'image':
              if (!empty($dataValue)) {
                $imageAlt = Html::escape($nodeArray['title']);
                $imageTitle = Html::escape($nodeArray['title']);

                if ((@unserialize($dataValue) !== FALSE)) {
                  $images = [];
                  foreach (unserialize($dataValue) as $row) {
                    $imas = $this->entityHelper->uploadImageMedia($row['name'], $imageAlt, $imageTitle, FALSE, $dirExtract, $prop['setting']);
                    $images = array_merge($images, $imas);
                  }

                  if ($images) {
                    $nodeArray[$dataKey] = $images;
                  }

                }
                else {
                  $imageSrc = $dataValue;
                  $images = $this->entityHelper->uploadImageMedia($imageSrc, $imageAlt, $imageTitle, FALSE, $dirExtract, $prop['setting']);
                  if ($images) {
                    $nodeArray[$dataKey] = $images;
                  }
                }
              }
              break;
            case 'file':
              $fileDescription = $nodeArray['title'];
              if ((@unserialize($dataValue) !== FALSE)) {
                $files = [];
                foreach (unserialize($dataValue) as $row) {
                  if (isset($row['description']) && !empty($row['description'])) {
                    $fileDescription = Html::escape($row['description']);
                  }
                  $fileItem = $this->entityHelper->uploadFileMedia($row['name'], $fileDescription, FALSE, $dirExtract, $prop['setting']);
                  if ($fileItem) {
                    $files[] = $fileItem;
                  }
                }
                if ($files) {
                  $nodeArray[$dataKey] = $files;
                }
              }
              /*$fileDescription = $nodeArray['title'];
              if (isset($itemValue['description']) && isset($itemValue['description']['key']) && !empty($dataValue)) {
                $fileDescription = Html::escape($dataValue);
              }*/

              /*$fileSrc = $data[$itemValue['key']];

              switch ($itemKey) {
                case 'field_gaceta_patente_file':
                case 'field_gaceta_signo_file':
                case 'field_gaceta_dautor_file':
                  if ($itemKey == 'field_gaceta_patente_file') {
                    switch ($fileDescription) {
                      case 'TOMO I':
                      case 'TOMO II':
                      case 'TOMO III':
                        $name = 'Tomo';
                        break;
                      default:
                        $name = 'Patentes';
                        break;
                    }
                  }

                  if ($itemKey == 'field_gaceta_signo_file') {
                    switch ($fileDescription) {
                      case 'TOMO I':
                      case 'TOMO II':
                      case 'TOMO III':
                        $name = 'Tomo';
                        break;
                      default:
                        $name = 'Signos';
                        break;
                    }
                  }
                  if ($itemKey == 'field_gaceta_dautor_file') {
                    switch ($fileDescription) {
                      case 'TOMO I':
                      case 'TOMO II':
                      case 'TOMO III':
                        $name = 'Tomo';
                        break;
                      default:
                        $name = 'Dautor';
                        break;
                    }
                  }
                  $date = new \DateTime($data[$keyIndex['field_gaceta_date']['key']]);
                  $attr = [
                    'name' => $name,
                    'number' => $data[$keyIndex['field_gaceta_number']['key']],
                    'date' => $date->format('dmY'),
                    'extension' => 'pdf',
                  ];
                  $files = $this->entityHelper->uploadFileMediaCustomName($fileSrc, $fileDescription, $dirExtract, $itemValue['properties']['setting'], $attr);
                  break;

                case 'field_oposicion_signo_file':
                case 'field_oposicion_patente_file':
                  if ($itemKey == 'field_oposicion_patente_file') {
                    $name = 'OP_Patentes';
                  }

                  if ($itemKey == 'field_oposicion_signo_file') {
                    $name = 'OP_Signos';
                  }

                  $title = $data[$keyIndex['field_oposicion_gaceta']['key']];

                  $node = \Drupal::entityQuery('node')
                    ->condition('type', 'gaceta')
                    ->condition('title', $title, 'IN')
                    ->execute();

                  if ($node) {
                    $node = Node::load(key($node));
                    $gaceta = $node->get('field_gaceta_number')
                      ->first()
                      ->getValue()['value'];
                    $gacetaDate = $node->get('field_gaceta_date')
                      ->first()
                      ->getValue()['value'];

                    $date = new \DateTime($gacetaDate);
                    $attr = [
                      'name' => $name,
                      'number' => $gaceta,
                      'date' => $date->format('dmY'),
                      'extension' => 'pdf',
                    ];
                    $files = $this->entityHelper->uploadFileMediaCustomName($fileSrc, $fileDescription, $dirExtract, $itemValue['properties']['setting'], $attr);
                  }
              }*/
              break;
            case 'entity_reference':
              if ($prop['setting']['target_type'] == 'taxonomy_term') {

                $voc = key($prop['setting']['handler_settings']['target_bundles']);
                if ($voc) {
                  if ((@unserialize($dataValue) !== FALSE)) {
                    $value = unserialize($dataValue);
                    $value = $value[0]['name'];
                  }
                  else {
                    $value = $dataValue;
                  }

                  $terms = $this->entityHelper->getTermReference($voc, $value);
                  if ($terms) {
                    $nodeArray[$dataKey] = $terms;
                  }
                }
              }

              if ($prop['setting']['target_type'] == 'node') {
                $bundle = key($prop['setting']['handler_settings']['target_bundles']);
                if ((@unserialize($dataValue) !== FALSE)) {
                  $value = unserialize($dataValue);
                  $value = $value[0]['name'];
                }
                else {
                  $value = $dataValue;
                }
                $node = \Drupal::entityQuery('node')
                  ->condition('type', $bundle)
                  ->condition('title', $value, 'IN')
                  ->execute();
                if ($node) {
                  $nodeArray[$dataKey][] = ['target_id' => key($node)];
                }
              }
              break;
            case 'text_long':
              if ((@unserialize($dataValue) !== FALSE)) {
                $nodeArray[$dataKey] = unserialize($dataValue);
              }
              else {
                $nodeArray[$dataKey] = [
                  'value' => $dataValue,
                  'format' => 'full_html',
                ];
              }
              break;
            case 'text_with_summary':
              if ((@unserialize($dataValue) !== FALSE)) {
                $nodeArray[$dataKey] = unserialize($dataValue);
              }
              else {
                $nodeArray[$dataKey] = [
                  'value' => $dataValue,
                  'summary' => "",
                  'format' => 'full_html',
                ];
              }
              break;
            case 'created':
              try {
                if (isset($data['created_datetime'])) {
                  $nodeArray[$dataKey] = strtotime($data['created_datetime']);
                }
                else {
                  $created = new \DateTime($dataValue);
                  $nodeArray[$dataKey] = $created->getTimestamp();
                }
              } catch (Exception $e) {
                if (isset($data['created_datetime'])) {
                  $nodeArray[$dataKey] = strtotime($data['created_datetime']);
                }
                else {
                  $nodeArray[$dataKey] = $dataValue;
                }
              }
              break;
            case 'geolocation':
            case 'layout_section':
            case 'map':
              $nodeArray[$dataKey] = unserialize($dataValue);
              break;
            case 'path':
              $nodeArray[$dataKey] = $dataValue;
              break;
            case 'integer':
              $nodeArray[$dataKey] = $dataValue;
              break;
            case 'string':
              $nodeArray[$dataKey] = Html::escape($dataValue);
              break;
            case 'boolean':
              $nodeArray[$dataKey] = (boolean) $dataValue;
              break;
            default:
              break;
          }
        }
      }

      if ($nodeArray['nid']) {
        unset($nodeArray['nid']);
      }


      $date->add(\DateInterval::createFromDateString('1 minutes'));
      $nodeArray['type'] = $contentTypes;

      $nodeArray['changed'] = $date->getTimestamp();

      $node = Node::create($nodeArray);
      $node->save();
      $nodeArray = [];
    }


    if (count($uploadZip) && $uploadZipFile) {
      $entries = $storage->loadMultiple([$uploadZipFile->id()]);
      $storage->delete($entries);
    }

    $entries = $storage->loadMultiple([$uploadCsvFile->id()]);
    $storage->delete($entries);

    \Drupal::messenger()->addMessage("Importado exitosamente.");
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxDependentDropdownCallback(array &$form, FormStateInterface $form_state) {
    return $form['content_types_fields'];
  }
}
