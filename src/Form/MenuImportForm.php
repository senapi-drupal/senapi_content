<?php


namespace Drupal\senapi_content\Form;


use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\senapi_content\ImportHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MenuImportForm extends FormBase {

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * A instance of the senapi_content helper services.
   *
   * @var \Drupal\senapi_content\ImportHelper
   */
  protected $entityHelper;

  /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    FileSystem $file_system,
    ImportHelper $entityHelper,
    EntityTypeManagerInterface $entityTypeManager) {

    $this->fileSystem = $file_system;
    $this->entityHelper = $entityHelper;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('senapi_content.import_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['upload_csv'] = [
      '#type' => 'managed_file',
      '#title' => 'Seleccione archivo CSV',
      '#description' => 'Por favor cargue el archivo CSV generado por la acción exportar.',
      '#upload_location' => $this->entityHelper->folders['import_data']['relative'],
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uploadCsv = $form_state->getValue('upload_csv');
    $storage = $this->entityTypeManager->getStorage('file');

    $uploadCsvFile = $storage->load($uploadCsv[0]);

    $handle = $this->entityHelper->getFileHandler($uploadCsvFile->getFileUri());

    $header = $this->entityHelper->getCsvData($handle);
    $result = 0;
    $operations = [];
    while (($data = $this->entityHelper->getCsvData($handle)) !== FALSE) {
      $data = array_combine($header, $data);
      $rowData = [
        'data' => $data,
        'result' => ++$result,
      ];

      $operations[] = [
        '\Drupal\senapi_content\ImportHelper::menuCallback',
        [$rowData],
      ];
    }

    $this->entityHelper->batchOprations($operations, $handle);

    $entries = $storage->loadMultiple([$uploadCsvFile->id()]);
    $storage->delete($entries);
  }
}