<?php


namespace Drupal\senapi_content\Form;


use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\senapi_content\ImportHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BatchImportForm extends FormBase {
 /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * A instance of the senapi_content helper services.
   *
   * @var \Drupal\senapi_content\ImportHelper
   */
  protected $entityHelper;

  /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    FileSystem $file_system,
    ImportHelper $entityHelper,
    EntityTypeManagerInterface $entityTypeManager) {

    $this->fileSystem = $file_system;
    $this->entityHelper = $entityHelper;
    $this->entityTypeManager = $entityTypeManager;
  }
    /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('senapi_content.import_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['dir'] = [
      '#type' => 'textfield',
      '#title' => 'Directorio de contenido',
      '#description' => 'Por favor ingrese directorio de contenido por defecto.',
      '#default_value' => '/media/datos/content',
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $dir = $form_state->getValue('dir');
    if (empty($dir)) {
      $form_state->setErrorByName('dir', 'Por favor ingrese directorio de contenido.');
    }

    if (!is_dir($dir)) {
      $form_state->setErrorByName('dir', 'El directorio no existe.');
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dirRoot = $form_state->getValue('dir');

    $dirs = [
      'node',
      'block_content',
      'menu'
    ];

    $stores = [];

    foreach ($dirs as $dir) {
      if (is_dir("$dirRoot/$dir")) {
        $stores[$dir] = array_values(preg_grep('/.csv/', scandir("$dirRoot/$dir")));
      }
    }
    $date = new \DateTime();
    foreach ($stores as $dir => $storage) {
      foreach ($storage as $item) {
        list($name) = explode('.csv', $item);
        $fileCsv = "$dirRoot/$dir/$name" . ".csv";
        $fileZip = "$dirRoot/$dir/$name" . ".zip";
        $this->entityHelper->batchPrepare($fileCsv, $fileZip, $name, $dir, $date);
      }
    }


    \Drupal::messenger()->addMessage(t("batch"));

  }
}