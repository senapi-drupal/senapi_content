<?php


namespace Drupal\senapi_content\Form;


use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\senapi_content\ImportHelper;
use Drupal\system\Entity\Menu;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MenuExportForm extends FormBase {
  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * A instance of the senapi_content helper services.
   *
   * @var \Drupal\senapi_content\ImportHelper
   */
  protected $entityHelper;

  /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    FileSystem $file_system,
    ImportHelper $entityHelper,
    EntityTypeManagerInterface $entityTypeManager) {

    $this->fileSystem = $file_system;
    $this->entityHelper = $entityHelper;
    $this->entityTypeManager = $entityTypeManager;
  }
    /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('senapi_content.import_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $menuObject = Menu::loadMultiple();

    $menuTypes = array_map(function ($menuEnt) {
      return $menuEnt->label();
    }, $menuObject);
    $selected = !empty($form_state->getValue('menu_types')) ? $form_state->getValue('menu_types') : key($menuTypes);

    $form['menu_types'] = [
      '#type' => 'select',
      '#title' => 'Exportar menu',
      '#description' => 'Por favor seleccione tipo de menu.',
      '#default_value' => $selected,
      '#options' => $menuTypes,
      '#states' => [

      ],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];

    return $form;
  }

    /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field = $form_state->getValue('menu_types');
    $filenameData = $field . '.csv';

    $pathData = $this->entityHelper->createCsvFileExportMenuData($filenameData, $field);

    $urlData = file_create_url($pathData);

    $response = 'Descargar <a href="' . $urlData . '" target="_blank">' . $filenameData . '</a> <br>';
    \Drupal::messenger()->addMessage(t($response));
  }
}