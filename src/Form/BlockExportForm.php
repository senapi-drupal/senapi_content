<?php


namespace Drupal\senapi_content\Form;


use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BlockExportForm extends FormBase {

  /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
    /**
   * Drupal entity field manager
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManager $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $blockTypes = $this->getBlockTypes();
    if (count($blockTypes) === 0) {
      return [
        '#markup' => $this->t('You have not created any block types yet. Go to the <a href=":url">block type creation page</a> to add a new block type.', [
          ':url' => Url::fromRoute('block_content.type_add')->toString(),
        ]),
      ];
    }

    $selected = !empty($form_state->getValue('block_types')) ? $form_state->getValue('block_types') : key($blockTypes);

    $form['block_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Block content'),
      '#description' => $this->t('The block content type.'),
      '#default_value' => $selected,
      '#options' => $blockTypes,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::ajaxDependentDropdownCallback',
        'wrapper' => 'dropdown-second-replace',
      ],
    ];


    $options = $this->getFields('block_content', 'basic');



      $form['block_types_fields'] = [
      '#type' => 'select',
      '#title' => $blockTypes[$selected] . ' ',
      '#description' => 'Por favor seleccione campos de tipo de bloque.',
      '#default_value' => $selected,
      '#options' => $options,
      '#multiple' => TRUE,
      '#size' => 10,
      '#states' => [
       ],
      '#prefix' => '<div id="dropdown-second-replace">',
      '#suffix' => '</div>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),

    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $blockType = $form_state->getValue('block_types');
    $fields = $form_state->getValue('block_types_fields');

    $filenameData = $blockType . '.csv';
    $pathData = $this->createCsvFileExportData($filenameData, $blockType, $fields);

    $urlData = file_create_url($pathData);


    $response = 'Descargar <a href="' . $urlData . '" target="_blank">' . $filenameData . '</a> <br>';

    \Drupal::messenger()->addMessage(t($response));
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxDependentDropdownCallback(array &$form, FormStateInterface $form_state) {
    return $form['block_types_fields'];
  }

  public function getFields($entity_type_id, $bundle) {
    $bundleFields = [];

    foreach ($this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle) as $field_name => $field_definition) {
      if (in_array($field_definition->getName(), ['content_translation_source', 'content_translation_outdated', 'content_translation_uid', 'content_translation_created', 'revision_log', 'default_langcode', 'revision_default','revision_translation_affected', 'revision_created', 'revision_user', 'revision_id', 'reusable', 'metatag', 'langcode'])) {
        continue;
      }
      if (!empty($field_definition->getTargetBundle()) && in_array($field_definition->getType(), ['entity_reference', 'file', 'image'])) {
        $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";
      } else {
        if (in_array($field_definition->getType(), [])) {
          $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";
        } else {
          if (!in_array($field_name, ['created', 'changed', 'map', 'language', ])) {
            $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";
          }
        }
      }
    }

    return $bundleFields;
  }
  public function getBlockTypes() {
    $types = \Drupal::entityTypeManager()
      ->getStorage('block_content_type')
      ->loadMultiple();

    $blockTypes = [];
    foreach ($types as $blockType) {
      $blockTypes[$blockType->id()] = $blockType->label();
    }

    return $blockTypes;
  }

  public function createCsvFileExportData($filename, $block_type, $fields) {
    $folder = "public://migrate/export-data/" . $block_type;
    $folderExport = \Drupal::service('file_system')->realpath("public://migrate/export-data");
    $pathAbsolute = $folderExport . "/$filename";
    $pathRelative = "public://migrate/export-data/$filename";

    file_unmanaged_delete($pathAbsolute);

    if (!is_dir($folder)) {
      mkdir($folder, 0777, TRUE);
      chmod($folder, 0777);
    }

    $nids = \Drupal::entityQuery('block_content')
      ->condition('type', $block_type)
      ->execute();


    $blocks = $this->entityTypeManager->getStorage('block_content')->loadMultiple($nids);

    $file = fopen($pathAbsolute, 'w');

    $column = array_keys($fields);
    fputcsv($file, $column);
    foreach ($blocks as  $block) {
      $row = [];
      foreach ($fields as $field) {
       $type = $block->getTypedData()->getDataDefinition()->getPropertyDefinition($field)->getType();
       switch ($type) {
         case 'image':
         case 'file':
           break;
         case 'entity_reference':
           $row[] = $block->get($field)->target_id;
           break;
         case 'list_string':
         case 'text_long':
           break;
         case 'text_with_summary':
           $row[] = serialize($block->get($field)->getValue());
           break;
         case 'datetime':
           $row[] = serialize($block->get($field)->getValue());
           break;
          case 'path':
            $row[] = $block->get($field)->alias;
            break;
          case 'status':
          case 'boolean':
          case 'string':
            $row[] = $block->get($field)->value;
            break;
          default:
            $row[] = $block->get($field)->value;
            break;
       }
     }

     fputcsv($file, $row);
    }

    fclose($file);

    return $pathRelative;

    /*$blockContent = \Drupal::entityTypeManager()
      ->getStorage('block_content')
      ->loadByProperties(['type' => $block_type]);*/
  }
}