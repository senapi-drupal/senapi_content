<?php
namespace Drupal\senapi_content\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\senapi_content\ImportHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeExportForm extends FormBase {

    /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * A instance of the senapi_content helper services.
   *
   * @var \Drupal\senapi_content\ImportHelper
   */
  protected $entityHelper;

  /**
   * A instance of the EntityTypeManagerInterface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    FileSystem $file_system,
    ImportHelper $entityHelper,
    EntityTypeManagerInterface $entityTypeManager) {

    $this->fileSystem = $file_system;
    $this->entityHelper = $entityHelper;
    $this->entityTypeManager = $entityTypeManager;
  }
    /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('senapi_content.import_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $contentTypes = $this->entityHelper->getAllContentTypes();
    $selected = !empty($form_state->getValue('content_types')) ? $form_state->getValue('content_types') : key($contentTypes);
    $form['content_types'] = [
      '#type' => 'select',
      '#title' => 'Tipo de Contenido',
      '#description' => 'Por favor seleccione tipo de contenido para importar.',
      '#default_value' => $selected,
      '#options' => $contentTypes,
      '#states' => [
      ],
      '#ajax' => [
        'callback' => '::ajaxDependentDropdownCallback',
        'wrapper' => 'dropdown-second-replace',
      ],
      '#required' => TRUE,
    ];

    $options = $this->entityHelper->getAllItemFields($selected, 'node');

    $form['content_types_fields'] = [
      '#type' => 'select',
      '#title' => $contentTypes[$selected] . ' campos',
      '#default_value' => $selected,
      '#options' => $options,
      '#multiple' => TRUE,
      '#size' => 10,
      '#states' => [
      ],
      '#prefix' => '<div id="dropdown-second-replace">',
      '#suffix' => '</div>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $contentTypes = $form_state->getValue('content_types');
    $fields = $form_state->getValue('content_types_fields');

    $filenameData = $contentTypes . '.csv';
    $pathData = $this->entityHelper->createCsvFileExportData($filenameData, $contentTypes, $fields);

    $filenameMedia = $contentTypes . '.zip';
    $pathMedia = $this->entityHelper->createTarballExportMedia($filenameMedia, $contentTypes);

    $urlData = file_create_url($pathData);
    $urlMedia = file_create_url($pathMedia);

    $response = 'Descargar <a href="' . $urlData . '" target="_blank">' . $filenameData . '</a> <br>';
    if (is_file($pathMedia)) {
      $response .= 'Descargar <a href="' . $urlMedia . '" target="_blank">' . $filenameMedia . '</a>';
    }
    \Drupal::messenger()->addMessage(t($response));
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxDependentDropdownCallback(array &$form, FormStateInterface $form_state) {
    return $form['content_types_fields'];
  }
}