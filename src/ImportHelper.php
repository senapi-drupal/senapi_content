<?php

namespace Drupal\senapi_content;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Utility\Token;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ImportHelper
 *
 * @package Drupal\senapi_content
 */
class ImportHelper {

  /**
   * Initialization directory.
   *
   * @var array
   */
  public $folders;

  /**
   * Drupal Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Drupal File System.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * Drupal query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * Drupal entity field manager
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * Token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Constructor for \Drupal\image_export_import\EntitySaveHelper class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity type manager.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   The Form Builder.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entityQuery
   *   The Query Builder.
   * @param \Drupal\Core\Entity\EntityFieldManager $fieldManager
   *   The Field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystem $file_system, QueryFactory $entityQuery, EntityFieldManager $fieldManager, Token $token) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->entityQuery = $entityQuery;
    $this->fieldManager = $fieldManager;
    $this->token = $token;
    $this->folders = $this->createFolderExportImport();
  }

  /**
   * Initialization of directories for export and import.
   *
   * @return array
   */
  public function createFolderExportImport() {
    $srcPublic = "public://migrate";
    $srcMigrate = $this->fileSystem->realpath($srcPublic);

    $folders = [
      "public_migrate" => ['relative' => $srcPublic, 'absolute' => $srcMigrate],
      "export_data" => [
        'relative' => $srcPublic . "/export-data",
        'absolute' => $srcMigrate . '/export-data',
      ],
      "export_media" => [
        'relative' => $srcPublic . "/export-media",
        'absolute' => $srcMigrate . '/export-media',
      ],
      "import_data" => [
        'relative' => $srcPublic . "/import-data",
        'absolute' => $srcMigrate . '/import-data',
      ],
      "import_media" => [
        'relative' => $srcPublic . "/import-media",
        'absolute' => $srcMigrate . '/import-media',
      ],
    ];

    if (!is_dir($folders['public_migrate']['absolute'])) {
      mkdir($folders['public_migrate']['absolute'], 0777, TRUE);
      chmod($folders['public_migrate']['absolute'], 0777);
    }

    if (!is_dir($folders['export_data']['absolute'])) {
      mkdir($folders['export_data']['absolute'], 0777, TRUE);
      chmod($folders['export_data']['absolute'], 0777);
    }

    if (!is_dir($folders['export_media']['absolute'])) {
      mkdir($folders['export_media']['absolute'], 0777, TRUE);
      chmod($folders['export_media']['absolute'], 0777);
    }

    if (!is_dir($folders['import_data']['absolute'])) {
      mkdir($folders['import_data']['absolute'], 0777, TRUE);
      chmod($folders['import_data']['absolute'], 0777);
    }

    if (!is_dir($folders['import_media']['absolute'])) {
      mkdir($folders['import_media']['absolute'], 0777, TRUE);
      chmod($folders['import_media']['absolute'], 0777);
    }

    return $folders;
  }

  /**
   * Export Type of Content csv file.
   *
   * @param $filename
   * @param $content_types
   * @param $fields
   *
   * @return string
   */
  public function createCsvFileExportData($filename, $content_types, $fields) {
    $pathAbsolute = $this->folders['public_migrate']['absolute'] . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative'] . "/$filename";

    file_unmanaged_delete($pathAbsolute);

    $folder = $this->folders['export_media']['absolute'] . "/$content_types";

    if (!is_dir($folder)) {
      mkdir($folder, 0777, TRUE);
      chmod($folder, 0777);
    }

    $nids = \Drupal::entityQuery('node')
      ->condition('type', $content_types)
      ->execute();
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);

    $file = fopen($pathAbsolute, 'w');

    $column = [];
    $column[0] = 'nid';
    $column[1] = 'title';
    $column[2] = 'created';
    $column[3] = 'created_datetime';
    $column = array_merge($column, array_keys($fields));

    fputcsv($file, $column);

    foreach ($nodes as $node) {
      $row = [];
      $row[0] = $node->get('nid')->value;
      $row[1] = $node->get('title')->value;
      $row[2] = $node->get('created')->value;
      $row[3] = date('Y-m-d H:i:s', $node->get('created')->value);

      foreach ($fields as $field) {
        $type = $node->getTypedData()
          ->getDataDefinition()
          ->getPropertyDefinition($field)
          ->getType();

        switch ($type) {
          case 'image':
          case 'file':
            if (!empty($node->get($field)->target_id) && ($values = $node->get($field)
                ->getValue()) && count($values) >= 1) {

              foreach ($node->get($field)
                         ->referencedEntities() as $key => $item) {
                $data = $this->entityTypeManager->getStorage($item->getEntityTypeId())
                  ->load($item->id());

                if (!empty($data)) {
                  $src = $this->fileSystem->realpath($data->getFileUri());

                  shell_exec("cp -r $src $folder");
                  $values[$key]['name'] = basename($data->getFileUri());
                }

              }
              $row[] = serialize($values);
            }
            else {
              $row[] = "";
            }
            break;

          case 'entity_reference':
            if (!empty($node->get($field)->target_id) && ($values = $node->get($field)
                ->getValue()) && count($values) >= 1) {

              foreach ($node->get($field)
                         ->referencedEntities() as $key => $item) {
                $data = $this->entityTypeManager->getStorage($item->getEntityTypeId())
                  ->load($item->id());

                if ($data) {
                  switch ($item->getEntityTypeId()) {
                    case 'node':
                      $values[$key]['name'] = $data->getTitle();
                      break;
                    default:
                      $values[$key]['name'] = $data->getName();
                      break;
                  }
                }
              }
              $row[] = serialize($values);
            }
            else {
              $row[] = "";
            }
            break;

          case 'list_string':
          case 'text_long':
            $row[] = $node->get($field)->value;
            break;

          case 'text_with_summary':
            $row[] = serialize($node->get($field)->getValue());
            break;

          case 'datetime':
            $values = $node->get($field)->getValue();
            if (count($values) > 1) {
              $val = [];
              foreach ($values as $value) {
                $val[] = $value['value'];
              }

              $row[] = implode("|", $val);
            }
            else {
              $row[] = $node->get($field)->value;
            }
            break;

          case 'path':
            $row[] = $node->get($field)->alias;
            break;

          case 'geolocation':
          case 'link':
          case 'layout_section':
          case 'map':
            $row[] = serialize($node->get($field)->getValue());
            break;

          case 'status':
          case 'boolean':
          case 'string':
            $row[] = $node->get($field)->value;
            break;

          default:
            $row[] = $node->get($field)->value;
            break;
        }
      }

      fputcsv($file, $row);
    }


    fclose($file);

    return $pathRelative;
  }

  /**
   * Export Menu in csv file.
   *
   * @param $filename
   * @param $menu
   *
   * @return string
   */
  public function createCsvFileExportMenuData($filename, $menu) {
    $pathAbsolute = $this->folders['public_migrate']['absolute'] . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative'] . "/$filename";

    if (file_exists($pathAbsolute)) {
      \Drupal::service('file_system')->delete($pathAbsolute);
    }

    $file = fopen($pathAbsolute, 'w');

    $column = [];
    $column[] = 'id';
    $column[] = 'uuid';
    $column[] = 'langcode';
    $column[] = 'bundle';
    $column[] = 'enabled';
    $column[] = 'title';
    $column[] = 'description';
    $column[] = 'menu_name';
    $column[] = 'link';
    $column[] = 'external';
    $column[] = 'rediscover';
    $column[] = 'weight';
    $column[] = 'expanded';
    $column[] = 'parent';
    $column[] = 'changed';
    $column[] = 'default_langcode';

    fputcsv($file, $column);

    $menu_tree = \Drupal::menuTree();
    $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu);

    $query = \Drupal::database()->select('menu_tree');
    $query->fields('menu_tree');
    for ($i = 1; $i <= 9; $i++) {
      $query->orderBy('p' . $i, 'ASC');
    }
    $query->condition('menu_name', $menu);

    if (!empty($parameters->expandedParents)) {
      $query->condition('parent', $parameters->expandedParents, 'IN');
    }

    $result = $query->execute()->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

    $ids = [];
    foreach ($result as $item) {
      if (preg_match('/\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/', $item['id'], $matches)) {
        $ids = $ids + \Drupal::entityQuery('menu_link_content')
            ->condition('menu_name', $menu)
            ->condition('uuid', $matches[0])
            ->execute();
      }
    }

    $menuLinks = MenuLinkContent::loadMultiple($ids);
    foreach ($menuLinks as $link) {
      $uri = $link->get('link');
      $row = [];
      $row[] = $link->get('id')->value;
      $row[] = $link->get('uuid')->value;
      $row[] = $link->get('langcode')->value;
      $row[] = $link->get('bundle')->value;
      $row[] = $link->get('enabled')->value;
      $row[] = $link->get('title')->value;
      $row[] = $link->get('description')->value;
      $row[] = $link->get('menu_name')->value;

      $value = $link->get('link')->first()->getValue();
      if (preg_match('/entity:node\/(\d+)/', $uri->first()
        ->getValue()['uri'], $matches)) {
        $node = Node::load($matches[1]);
        $value['title'] = $node->getTitle();
      }

      $row[] = serialize([$value]);
      $row[] = $link->get('external')->value;
      $row[] = $link->get('rediscover')->value;
      $row[] = $link->get('weight')->value;
      $row[] = $link->get('expanded')->value;
      $row[] = $link->get('parent')->value;
      $row[] = $link->get('changed')->value;
      $row[] = $link->get('default_langcode')->value;

      fputcsv($file, $row);
    }

    fclose($file);

    return $pathRelative;
  }


  /**
   * Compress files.
   *
   * @param $filename
   * @param $content_type
   *
   * @return string
   */
  public function createTarballExportMedia($filename, $content_type) {
    $pathAbsolute = $this->folders['public_migrate']['absolute'] . "/$filename";
    $pathRelative = $this->folders['public_migrate']['relative'] . "/$filename";
    $mediaPath = $this->folders['export_media']['absolute'] . "/$content_type";

    file_unmanaged_delete($pathAbsolute);

    //$archiver = new ArchiveTar($pathAbsolute, 'gz');
    //$archiver->addModify(["$mediaPath"], "$content_type", "$mediaPath");

    $archiver = new \ZipArchive();
    $archiver->open($pathAbsolute, \ZipArchive::CREATE);
    $handle = opendir($mediaPath);
    while ($f = readdir($handle)) {
      if ($f != "." && $f != "..") {
        if (is_file($mediaPath . "/" . $f)) {
          $archiver->addFile($mediaPath . "/" . $f, $content_type . "/" . basename($f));
        }
      }
    }
    $archiver->close();

    file_unmanaged_delete_recursive($mediaPath);

    return $pathRelative;
  }

  /**
   * Import menu from csv file.
   */
  public static function menuCallback($row, &$context) {
    $data = $row['data'];

    \Drupal::logger('senapi_content')
      ->info('Menu import name: @menu_name  menu title: @title, menu uuid: @uuid', [
        '@menu_name' => $data['menu_name'],
        '@title' => $data['title'],
        '@uuid' => $data['uuid'],
      ]);

    $menuName = $data['menu_name'];
    $menu = \Drupal::entityTypeManager()
      ->getStorage('menu')
      ->load($menuName);

    if (empty($menu)) {
      \Drupal::entityTypeManager()
        ->getStorage('menu')
        ->create([
          'id' => $menuName,
          'label' => $menuName,
          'expanded' => TRUE,
        ])->save();
    }

    $menuLinkEntity = \Drupal::entityQuery('menu_link_content')
      ->condition('uuid', $data['uuid'])
      ->execute();

    if (!$menuLinkEntity) {
      $value = unserialize($data['link']);
      if (preg_match('/entity:node\/(\d+)/', $value[0]['uri'], $matches)) {
        $nodes = \Drupal::entityTypeManager()
          ->getStorage('node')
          ->loadByProperties(['title' => $value[0]['title']]);
        if (count($nodes) > 0) {
          $value[0]['uri'] = 'entity:node/' . array_shift($nodes)->id();
        }
        else {
          $value[0]['uri'] = 'internal:#';
        }
      }

      $menuLinkEntity = MenuLinkContent::create([
        'uuid' => $data['uuid'],
        'langcode' => $data['langcode'],
        'enabled' => $data['enabled'],
        'title' => $data['title'],
        'description' => $data['description'],
        'menu_name' => $data['menu_name'],
        'link' => $value,
        'external' => $data['external'],
        'rediscover' => $data['rediscover'],
        'weight' => $data['weight'],
        'expanded' => $data['expanded'],
        'parent' => $data['parent'],
        //'changed' => $data['changed'],
        //'default_langcode' => $data['default_langcode'],
      ]);

      $operationDetails = ' Importado exitosamente.';
    }
    else {
      $menuLinkEntity = MenuLinkContent::load(reset($menuLinkEntity));
      $operationDetails = ' Actualizado exitosamente.';
    }

    $menuLinkEntity->save();
    unset($menuLinkEntity);

    $context['message'] = t('Ejecutando lote "@id" @details', [
      '@id' => $data['title'],
      '@details' => $operationDetails,
    ]);
    $context['results'] = $row['result'];
  }


  /**
   * Import block from csv file.
   */
  /**
   * @param $row
   * @param $context
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function blockContentCallback($row, &$context) {
    $data = $row['data'];

    \Drupal::logger('senapi_content')
      ->info('Block import type: @type  block info: @info, block uuid: @uuid', [
        '@type' => $data['type'],
        '@info' => $data['info'],
        '@uuid' => $data['uuid'],
      ]);

    if ((@unserialize($data['body']) !== FALSE)) {
      $body = unserialize($data['body']);
      $content = [
        'uuid' => $data['uuid'],
        'info' => $data['info'],
        'type' => $data['type'],
        'body' => $body[0],
      ];

    }
    else {

      $content = [
        'uuid' => $data['uuid'],
        'info' => $data['info'],
        'type' => $data['type'],
        'body' => [
          'value' => $data['body_value'],
          'format' => $data['body_format'],
        ],
      ];
    }

    $block_content = \Drupal::entityTypeManager()->getStorage('block_content')
      ->create(
        $content
      );

    $block_content->save();

    //$this->storeCreatedContentUuids([$block_content->uuid() => 'block_content']);

    $operationDetails = ' Importación exitosamente.';

    $context['message'] = t('Ejecutando lote "@id" @details', [
      '@id' => $data['info'],
      '@details' => $operationDetails,
    ]);
    $context['results'] = $row['result'];
  }

  /**
   * @param $row
   * @param $context
   */
  public static function nodeCallback($row, &$context) {
    $ref = $row['reference'];
    $data = $row['data'];
    $changed = $row['changed'];
    $dir = $row['dir'];
    $type = $row['type'];
    $fields = $row['fields'];
    $fieldNames = $fields['name'];
    $fieldProperties = $fields['properties'];

    \Drupal::logger('senapi_content')
      ->info('Node import type: @type title: @title', [
        '@type' => $row['type'],
        '@title' => $data['title'],
      ]);

    $node = [];
    foreach ($data as $dataKey => $dataValue) {
      if (in_array($dataKey, $fieldNames)) {
        $prop = $fieldProperties[$dataKey];

        switch ($prop['type']) {
          case 'image':
            if (!empty($dataValue)) {
              if ((@unserialize($dataValue) !== FALSE)) {
                $images = [];
                foreach (unserialize($dataValue) as $rowKey => $row) {
                  $number = str_pad(++$rowKey, 2, 0, STR_PAD_LEFT);
                  if ($prop['setting']['alt_field_required']) {
                    if (empty($row['alt'])) {
                      $imageAlt = Html::escape($row['title']);
                    }
                    else {
                      $imageAlt = Html::escape($node['title']);
                    }
                    $imageAlt = 'img ' . $number . ' ' . $imageAlt;

                  }
                  else {
                    $imageAlt = FALSE;
                  }


                  if ($prop['setting']['title_field_required']) {
                    if (empty($row['title'])) {
                      $imageTitle = Html::escape($row['title']);
                    }
                    else {
                      $imageTitle = Html::escape($node['title']);
                    }
                    $imageTitle = 'img ' . $number . ' ' . $imageTitle;
                  }
                  else {
                    $imageTitle = FALSE;
                  }

                  $imageName = @self::parseFilename($type, $row, $number, $prop, $data, $node);
                  $imageAlt = $imageName;
                  $imas = @self::uploadImageMedia($row['name'], $imageAlt, $imageTitle, $imageName
                    , $dir, $prop['setting']);
                  if ($imas) {
                    $images[] = $imas;
                  }
                }

                if ($images) {
                  $node[$dataKey] = $images;
                }

              }
              else {
                $imageSrc = $dataValue;
                $imageTitle = 'img title ' . Html::escape($node['title']);
                $imageName = @self::parseFilename($type, $row, '01', $prop, $data, $node);
                $imageAlt = $imageName;

                $imas = @self::uploadImageMedia($imageSrc, $imageAlt, $imageTitle, $imageName, $dir, $prop['setting']);
                if ($imas) {
                  $node[$dataKey] = $imas;
                }
              }
            }
            break;
          case 'file':
            if ((@unserialize($dataValue) !== FALSE)) {
              $files = [];
              foreach (unserialize($dataValue) as $rowKey => $row) {
                $number = str_pad(++$rowKey, 2, 0, STR_PAD_LEFT);
                if ($prop['setting']['description_field']) {
                  /*$fileDescription = $node['title'];
                  if (isset($row['description']) && !empty($row['description'])) {
                    $fileDescription = Html::escape($row['description']);
                  }*/

                  $fileName = @self::parseFilename($type, $row, $number, $prop, $data, $node);
                  $fileDescription = $fileName;
                }
                else {
                  $fileDescription = FALSE;
                  $fileName = FALSE;
                }
                $fileItem = @self::uploadFileMedia($row['name'], $fileDescription, $fileName, $dir, $prop['setting']);
                if ($fileItem) {
                  $files[] = $fileItem;
                }
              }
              if ($files) {
                $node[$dataKey] = $files;
              }
            }

            break;
          case 'entity_reference':
            if ($prop['setting']['target_type'] == 'taxonomy_term') {

              $voc = key($prop['setting']['handler_settings']['target_bundles']);
              if ($voc) {
                if ((@unserialize($dataValue) !== FALSE)) {
                  $value = unserialize($dataValue);
                  $value = $value[0]['name'];
                }
                else {
                  $value = $dataValue;
                }

                $terms = @self::getTermReference($voc, $value);
                if ($terms) {
                  $node[$dataKey] = $terms;
                }
              }
            }

            if ($prop['setting']['target_type'] == 'node') {
              $bundle = key($prop['setting']['handler_settings']['target_bundles']);
              if ((@unserialize($dataValue) !== FALSE)) {
                $value = unserialize($dataValue);
                $value = $value[0]['name'];
              }
              else {
                $value = $dataValue;
              }
              $nod = \Drupal::entityQuery('node')
                ->condition('type', $bundle)
                ->condition('title', $value, 'IN')
                ->execute();
              if ($nod) {
                $node[$dataKey][] = ['target_id' => key($nod)];
              }
            }
            break;
          case 'text_long':
            if ((@unserialize($dataValue) !== FALSE)) {
              $node[$dataKey] = unserialize($dataValue);
            }
            else {
              $node[$dataKey] = [
                'value' => $dataValue,
                'format' => 'full_html',
              ];
            }
            break;
          case 'text_with_summary':
            if ((@unserialize($dataValue) !== FALSE)) {
              $node[$dataKey] = unserialize($dataValue);
            }
            else {
              $node[$dataKey] = [
                'value' => $dataValue,
                'summary' => "",
                'format' => 'full_html',
              ];
            }
            break;
          case 'created':
            try {
              $created = new \DateTime($dataValue);
              if (isset($data['created_datetime'])) {
                $node[$dataKey] = strtotime($data['created_datetime']);
              }
              else {
                $node[$dataKey] = $created->getTimestamp();;
              }
            } catch (\Exception $e) {
              if (isset($data['created_datetime'])) {
                $node[$dataKey] = strtotime($data['created_datetime']);
              }
              else {
                $node[$dataKey] = $dataValue;
              }
            }
            break;
          case 'geolocation':
          case 'layout_section':
          case 'map':
            $node[$dataKey] = unserialize($dataValue);
            break;
          case 'path':
            $node[$dataKey] = $dataValue;
            break;
          case 'integer':
            $node[$dataKey] = $dataValue;
            break;
          case 'string':
            $node[$dataKey] = Html::escape($dataValue);
            break;
          case 'boolean':
            $node[$dataKey] = (boolean) $dataValue;
            break;
          default:
            break;
        }

      }
    }

    if ($node['nid']) {
      unset($node['nid']);
    }


    $node['type'] = $type;

    $node['changed'] = $changed;

    $node = Node::create($node);
    $node->save();


    $operationDetails = ' Importación exitosamente.';

    $context['message'] = t('Ejecutando lote "@id" @details', [
      '@id' => $type,
      '@details' => $operationDetails,
    ]);

    $context['results'] = $row['result'];
  }

  /**
   * @param $image_src
   * @param $image_alt
   * @param $image_title
   * @param $image_name
   * @param $dir_extract
   * @param $setting
   *
   * @return array|null
   */
  public static function uploadImageMedia($image_src, $image_alt, $image_title, $image_name, $dir_extract, $setting) {
    $destination = @self::getUploadLocation($setting);
    \Drupal::logger('senapi_content')
      ->info('Image src @image_src, Image extract @dir_extract, Image destination @destination', [
        '@image_src' => $image_src,
        '@dir_extract' => $dir_extract,
        '@destination' => $destination,
      ]);

    if (!\Drupal::service('file_system')
      ->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY)) {
      throw new HttpException(500, 'La ruta del archivo de destino no se puede escribir.');
    }

    @chmod($destination, 0777);

    $uriFile = NULL;
    foreach (file_scan_directory($dir_extract, "/^$image_src$/") as $uri => $file) {
      $uriFile = $uri;
    }
    if (!empty($uriFile)) {
      if ($image_alt != FALSE) {
        $aliasService = \Drupal::service('pathauto.alias_cleaner');
        if ($image_name != FALSE) {
          $nameAlias = $aliasService->cleanString($image_name);
        }
        else {
          $nameAlias = $aliasService->cleanString($image_alt);
        }

        $fileName = join('.', [
          $nameAlias,
          pathinfo($image_src, PATHINFO_EXTENSION),
        ]);

        $file = file_save_data(file_get_contents($uriFile), $destination . '/' . $fileName, FileSystemInterface::EXISTS_RENAME);

        @self::exiftool(@self::removeAccents($nameAlias), \Drupal::service('file_system')->realpath($file->getFileUri()));

        $fileArray = [
          'target_id' => $file->id(),
          'alt' => mb_strtoupper($image_alt),
        ];

        if ($image_title != FALSE) {
          $fileArray['title'] = mb_strtoupper($image_title);
        }
      }
      else {
        $file = file_save_data(file_get_contents($uriFile), $destination . '/' . $image_src, FileSystemInterface::EXISTS_RENAME);

        @self::exiftool(@self::removeAccents($image_src), \Drupal::service('file_system')->realpath($file->getFileUri()));

        $fileArray = [
          'target_id' => $file->id(),
        ];
      }

      @unlink($uriFile);

      return $fileArray;
    }

    return NULL;
  }

  /**
   * @param $file_src
   * @param $file_description
   * @param $file_name
   * @param $dir_extract
   * @param $setting
   *
   * @return array|null
   */
  public static function uploadFileMedia($file_src, $file_description, $file_name, $dir_extract, $setting) {
    $destination = @self::getUploadLocation($setting);
    if (!\Drupal::service('file_system')
      ->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY)) {
      throw new HttpException(500, 'La ruta del archivo de destino no se puede escribir.');
    }

    @chmod($destination, 0777);

    \Drupal::logger('senapi_content')
      ->info('File src @file_src, File extract @dir_extract, File destination @destination', [
        '@file_src' => $file_src,
        '@dir_extract' => $dir_extract,
        '@destination' => $destination,
      ]);

    $uriFile = NULL;
    foreach (file_scan_directory($dir_extract, "/^$file_src$/") as $uri => $file) {
      $uriFile = $uri;
    }

    if (!empty($uriFile)) {
      if ($file_description != FALSE) {
        $aliasService = \Drupal::service('pathauto.alias_cleaner');
        if ($file_name != FALSE) {
          $nameAlias = $aliasService->cleanString($file_name);
        }
        else {
          $nameAlias = $aliasService->cleanString($file_description);
        }
        $fileName = join('.', [
          $nameAlias,
          pathinfo($file_src, PATHINFO_EXTENSION),
        ]);
        $file = file_save_data(file_get_contents($uriFile), $destination . '/' . $fileName, FileSystemInterface::EXISTS_RENAME);

        @self::exiftool(@self::removeAccents($nameAlias), \Drupal::service('file_system')->realpath($file->getFileUri()));

        $fileArray = [
          'target_id' => $file->id(),
          'description' => $file_description,
        ];
      }
      else {
        $file = file_save_data(file_get_contents($uriFile), $destination . '/' . $file_src, FileSystemInterface::EXISTS_RENAME);

        @self::exiftool(@self::removeAccents($file_src), \Drupal::service('file_system')->realpath($file->getFileUri()));

        $fileArray = [
          'target_id' => $file->id(),
        ];
      }

      @unlink($uriFile);

      return $fileArray;
    }
    return NULL;
  }


   public static function exiftool($title, $file) {
     $creator = 'Servicio Nacional de Propiedad Intelectual';
     $command = sprintf('exiftool -r -overwrite_original -P -all:all= -Creator=%s -Title=%s %s', escapeshellarg($creator), escapeshellarg(mb_strtoupper($title)), escapeshellarg($file));

     $output = []; $return = null; exec($command, $output, $return);

   }

  public static function removeAccents($text) {
    $text = strtr(utf8_decode($text), utf8_decode('°'), 'ro');
    return strtr(utf8_decode($text), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
  }

  /**
   * Batch completed.
   */
  public static function importFromCsvFinishedCallback($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('@count procesados.', ['@count' => $results]));
    }
    else {
      $messenger->addMessage(t('Finalizado con error.'));
    }
  }


  /**
   * Get Upload Location Directory.
   *
   * @param array $settings
   *
   * @return string
   */
  public static function getUploadLocation(array $settings) {
    $destination = trim($settings['file_directory'], '/');

    $destination = PlainTextOutput::renderFromHtml(\Drupal::service('token')
      ->replace($destination, []));
    return $settings['uri_scheme'] . '://' . $destination;
  }


  /**
   * Get list of content types.
   *
   * @return array
   */
  public function getAllContentTypes() {
    $contentTypes = NodeType::loadMultiple();

    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    return $contentTypesList;
  }

  /**
   * Get Content Type Fields.
   *
   * @param string $entity_type
   * @param null $content_type
   *
   * @return array
   */
  public function getFields($content_type, $entity_type = 'node') {
    $fields = [];

    if (empty($content_type)) {
      return $fields;
    }

    foreach ($this->fieldManager->getFieldDefinitions($entity_type, $content_type) as $fieldDefinition) {
      if ($fieldDefinition->isRequired()) {
        $fields['requireField'][$fieldDefinition->getName()] = $fieldDefinition->getName();
      }

      if (!empty($fieldDefinition->getTargetBundle()) && in_array($fieldDefinition->getType(), [
          'image',
          'entity_reference',
          'file',
        ])) {


        $fields['name'][$fieldDefinition->getName()] = $fieldDefinition->getName();
        $fields['properties'][$fieldDefinition->getName()] = [
          'name' => $fieldDefinition->getName(),
          'type' => $fieldDefinition->getType(),
          'setting' => $fieldDefinition->getSettings(),
          'require' => $fieldDefinition->isRequired(),
          'list' => $fieldDefinition->isList(),
        ];
      }
      else {
        if (in_array($fieldDefinition->getType(), [
          'datetime',
          'list_string',
          'text_long',
        ])) {
          $fields['name'][$fieldDefinition->getName()] = $fieldDefinition->getName();
          $fields['properties'][$fieldDefinition->getName()] = [
            'name' => $fieldDefinition->getName(),
            'type' => $fieldDefinition->getType(),
            'setting' => $fieldDefinition->getSettings(),
            'require' => $fieldDefinition->isRequired(),
            'list' => $fieldDefinition->isList(),
          ];
        }
        else {
          $fields['name'][$fieldDefinition->getName()] = $fieldDefinition->getName();
          $fields['properties'][$fieldDefinition->getName()] = [
            'name' => $fieldDefinition->getName(),
            'type' => $fieldDefinition->getType(),
            'setting' => $fieldDefinition->getSettings(),
            'require' => $fieldDefinition->isRequired(),
            'list' => $fieldDefinition->isList(),
          ];
        }
      }
    }

    return $fields;
  }

  /**
   * Get Content Type Fields.
   *
   * @param $bundle
   * @param $entity_type_id
   *
   * @return array
   */
  public function getAllItemFields($bundle, $entity_type_id) {
    $bundleFields = [];

    foreach ($this->fieldManager->getFieldDefinitions($entity_type_id, $bundle) as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && in_array($field_definition->getType(), [
          'image',
          'entity_reference',
          'file',
        ])) {

        $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";

      }
      else {
        if (in_array($field_definition->getType(), [
          'datetime',
          'list_string',
          'text_long',
        ])) {
          $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";
        }
        else {
          if (!in_array($field_name, [
            'menu_link',
            'uuid',
            'uid',
            'nid',
            'vid',
            'langcode',
            'title',
            'type',
            'created',
            'changed',
            //'promote',
            'sticky',
            'default_langcode',
            //'path',
            'comment',
            'revision_log',
            'revision_uid',
            'revision_timestamp',
            'revision_langcode',
            'revision_default',
            'revision_translation_affected',
          ])) {
            $bundleFields[$field_name] = $field_definition->getLabel() . ' - ' . $field_definition->getName() . "(" . $field_definition->getType() . ")";
          }
        }
      }
    }

    return $bundleFields;
  }

  /**
   * Get reference of taxonomy.
   *
   * @param $voc
   * @param $terms
   *
   * @return array
   */
  public static function getTermReference($voc, $terms) {
    $vocName = strtolower($voc);
    $vid = preg_replace('@[^a-z0-9_]+@', '_', $vocName);
    $vocabularies = Vocabulary::loadMultiple();


    if (!isset($vocabularies[$vid])) {
      // Create Vocabulary
      @self::createVoc($terms, $vid);
    }

    $termArray = array_map('trim', explode('|', $terms));

    $termIds = [];

    foreach ($termArray as $term) {
      $termId = @self::getTermId($term, $vid);
      if (empty($termId)) {
        $termId = @self::createTerm($voc, $term, $vid);
      }
      $termIds[]['target_id'] = $termId;
    }
    return $termIds;
  }

  public static function parseFilename($type, $row, $index, $prop, $data, $node) {
    $name = FALSE;
    $nameData = [];
    switch ($type) {
      case 'gaceta':
        $tomo = [
          'TOMO I',
          'TOMO II',
          'TOMO III',
        ];

        switch ($prop['name']) {
          case 'field_file': #signo
            $name = 'signos';
            break;
          case 'field_file_second': #patente
            $name = 'patentes';
            break;
          case 'field_file_third': #dautor
            $name = 'dautor';
            break;
        }

        if (in_array($row['description'], $tomo)) {
          $name = 'tomo';
        }

        array_push($nameData, $type);


        if (isset($data['field_number'])) {
          array_push($nameData, $data['field_number']);
        }
        array_push($nameData, $name);

        if (isset($node['created'])) {
          array_push($nameData, date('Ymd', $node['created']));
        }
        $name = join(' ', $nameData);
        break;
      case 'oposicion':
        array_push($nameData, $type);
        array_push($nameData, 'gaceta');

        $title = array_filter(array_map(function ($value) {
              if (!empty($value)) {
                if ($value != 'OP') {
                  return $value;
                }
              }
            }, preg_split('/([_])|(?<=\D)(?=\d)|(?<=\d)(?=\D)/', pathinfo($row['name'], PATHINFO_FILENAME))
            )
          )
        ;

        array_push($nameData, $title[2]);
        array_push($nameData, $title[1]);
        if (isset($node['created'])) {
          array_push($nameData, date('Ymd', $node['created']));
        }
        $name = join(' ', $nameData);
        break;
      case 'auditoria':
        $title = join(' ', array_filter(array_map(function ($value) {
          if (!empty($value)) {
            return str_replace('_', '', $value);
          }
        }, preg_split('/(?=[A-Z]|_)/', pathinfo($row['name'], PATHINFO_FILENAME)))));

        array_push($nameData, $type);
        if (isset($node['created'])) {
          array_push($nameData, date('Ymd', $node['created']));
        }

        array_push($nameData, $index);
        array_push($nameData, $title);

        $name = join(' ', $nameData);
        break;
      case 'prensa':
        array_push($nameData, $type);

        if (isset($node['created'])) {
          array_push($nameData, date('Ymd', $node['created']));
        }
        array_push($nameData, $index);
        array_push($nameData, $node['title']);
        $name = join(' ', $nameData);
        break;
      case 'boletin':
      case 'fondo':
      case 'revista':
      case 'memoria':
      case 'transferencia':
        array_push($nameData, $type);
        if (isset($node['created'])) {
          array_push($nameData, date('Ymd', $node['created']));
        }
        array_push($nameData, $index);
        array_push($nameData, $node['title']);
        $name = join(' ', $nameData);
        break;

      case 'page':
        switch ($prop['name']) {
          case 'field_image': #signo
            array_push($nameData, $type);
            if (isset($node['created'])) {
              array_push($nameData, date('Ymd', $node['created']));
            }
            array_push($nameData, $index);
            array_push($nameData, $node['title']);
            $name = join(' ', $nameData);
            break;
        }
        break;
    }

    return $name;
  }

  /**
   * Create terms if it is not available.
   *
   * @param $vid
   * @param $voc
   */
  public static function createVoc($vid, $voc) {
    $vocabulary = Vocabulary::create([
      'vid' => $vid,
      'machine_name' => $vid,
      'name' => $voc,
    ]);

    $vocabulary->save();
  }

  /**
   * Get valid TermId.
   *
   * @param $term
   * @param $vid
   *
   * @return int|string|null
   */
  public static function getTermId($term, $vid) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vid);
    $query->condition('name', $term);

    $termResult = $query->execute();
    return key($termResult);
  }

  /**
   * Create terms if it is not available.
   *
   * @param $voc
   * @param $term
   * @param $vid
   *
   * @return int|string|null
   */
  public static function createTerm($voc, $term, $vid) {
    Term::create([
      'parent' => [$voc],
      'name' => $term,
      'vid' => $vid,
    ])->save();

    $termId = @self::getTermId($term, $vid);
    return $termId;
  }

  /**
   * Get row of CSV file.
   *
   * @param resource $handle
   *   Resource handler.
   *
   * @return array
   *   Array of csv row.
   */
  public function getCsvData($handle) {
    return fgetcsv($handle, 0, ',', '"');
  }

  /**
   * Get file handle from uri.
   *
   * @param string $uri
   *   URI of file.
   *
   * @return resource
   *   Resource handler.
   */
  public function getFileHandler($uri) {
    return fopen($this->fileSystem->realpath($uri), "r");
  }

  /**
   * Remove dir content type
   */
  public function rmdir($uri) {
    file_unmanaged_delete_recursive($this->fileSystem->realpath($uri));
  }

  public function batchOprations($operations, $handle, $name = "") {
    if (count($operations)) {
      $batch = [
        'title' => t('Importando archivo ' . $name),
        'operations' => $operations,
        'finished' => '\Drupal\senapi_content\ImportHelper::importFromCsvFinishedCallback',
        'error_message' => t('La importación ha encontrado un error.'),
        'progress_message' => t('Importado @current de @total fila.'),
      ];
      batch_set($batch);
      fclose($handle);
    }
  }

  /**
   * Get an appropriate archiver class for the file.
   *
   * @param string $file
   *   The file path.
   */
  public function getArchiver($file) {
    $extension = strstr(pathinfo($file)['basename'], '.');
    switch ($extension) {
      case '.tar.gz':
      case '.tar':
        $this->archiver = new \PharData($file);
        break;

      case '.zip':
        $this->archiver = new \ZipArchive($file);
        $this->archiver->open($file);
      default:
        break;
    }
    return $this->archiver;
  }

  /**
   * @param $file_csv
   * @param $file_zip
   * @param $type
   * @param $callback
   * @param \DateTime $date
   * @param bool $form
   *
   * @return $this
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function batchPrepare($file_csv, $file_zip, $type, $callback, \DateTime &$date, $form = FALSE) {
    if ($callback == NULL) {
      return $this;
    }
    if (!file_exists($file_csv)) {
      return $this;
    }

    if ($callback == 'node' && !in_array($type, array_keys($this->getAllContentTypes()))) {
      return $this;
    }

    if ($file_zip && file_exists($file_zip)) {
      $archive = $this->getArchiver($this->fileSystem->realpath($file_zip));
      $archive->extractTo($this->folders['import_media']['relative']);
    }

    $dirExtract = $this->folders['import_media']['relative'] . '/' . $type;
    if (!is_dir($dirExtract)) {
      $dirExtract = $this->folders['import_media']['relative'];
    }

    $method = lcfirst(implode('', array_map('ucfirst', explode('_', $callback))));

    $storage = $this->entityTypeManager->getStorage('file');
    $uploadCsvFile = FALSE;
    if ($form) {
      $uploadCsvFile = $storage->load($file_csv);
      $handle = $this->getFileHandler($uploadCsvFile->getFileUri());
    }
    else {
      $handle = $this->getFileHandler($file_csv);
    }

    $header = $this->getCsvData($handle);

    $fields = [];
    switch ($callback) {
      case 'node':
        $fields = $this->getFields($type);
        unset($fields['requireField']['type']);
        foreach ($fields['requireField'] as $field) {
          if (!in_array($field, $header)) {
            \Drupal::messenger()
              ->addError("Error al importar campos requeridos incompletos.");
          }
        }
        break;
    }

    $result = 0;
    $operations = [];

    while (($data = $this->getCsvData($handle)) !== FALSE) {
      $data = array_combine($header, $data);
      $date->add(\DateInterval::createFromDateString('1 minutes'));
      $rowData = [
        'data' => $data,
        'type' => $type,
        'changed' => $date->getTimestamp(),
        'result' => ++$result,
        'fields' => $fields,
        'dir' => $dirExtract,
      ];

      $operations[] = [
        '\Drupal\senapi_content\ImportHelper::' . $method . 'Callback',
        [$rowData],
      ];
    }

    $this->batchOprations($operations, $handle, 'CSV');

    if ($form && $uploadCsvFile) {
      $entries = $storage->loadMultiple([$uploadCsvFile->id()]);
      $storage->delete($entries);
    }
  }
}
