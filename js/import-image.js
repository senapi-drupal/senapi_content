(function ($, Drupal, drupalSettings) {
    'use strict';
    Drupal.behaviors.senapi_content = {
      attach: function (context, settings) {
          // Hide file field incase of export.
          if ($('.form-item-action-type select option:selected').val() === 'export') {
              $('.form-item-upload-csv').hide();
              $('.form-item-upload-zip').hide();
          }
          if ($('.form-item-action-type select option:selected').val() === 'delete') {
              $('.form-item-upload-csv').hide();
              $('.form-item-upload-zip').hide();
          }
        if ($('.form-item-action-type select option:selected').val() === 'export_menu') {
          $('.form-item-upload-csv').hide();
          $('.form-item-upload-zip').hide();
        }

          // Manage form based on action type.
          $('.form-item-action-type select', context).once().on('change', function () {
              if (this.value === 'import_menu') {
                $('.form-item-upload-csv').show();
              }else if (this.value === 'import') {
                  $('.form-item-upload-csv').show();
                  $('.form-item-upload-zip').show();
              }
              else {
                  $('.form-item-upload-csv').hide();
                  $('.form-item-upload-zip').hide();
              }
          });
      }
    };
})(jQuery, Drupal, drupalSettings);